library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity ID_EXEMEM is
    port ( 	reset                	: in  std_logic;
			clk                  	: in  std_logic;
			flush_in           		: in  std_logic;
			sig_insn_in				: in std_logic_vector(15 downto 0);
			sig_read_data_a_in 		: in std_logic_vector(15 downto 0);
			sig_read_data_b_in 		: in std_logic_vector(15 downto 0);
			reg_dst_in 				: in std_logic; 
			reg_write_in 			: in std_logic;
			alu_src_in 				: in std_logic;
			mem_write_in 			: in std_logic;
			mem_to_reg_in 			: in std_logic;
			alu_op_in 				: in std_logic_vector(0 to 3);	  
			branch_in 				: in std_logic;
			mem_loadfile_in 		: in std_logic; 
			register_a_in			: in std_logic_vector(0 to 3);
			register_b_in			: in std_logic_vector(0 to 3);
			
			sig_insn_out			: out std_logic_vector(15 downto 0);
			sig_read_data_a_out 	: out std_logic_vector(15 downto 0);
			sig_read_data_b_out 	: out std_logic_vector(15 downto 0);
			reg_dst_out 			: out std_logic; 
			reg_write_out 			: out std_logic;
			alu_src_out 			: out std_logic;
			mem_write_out 			: out std_logic;
			mem_to_reg_out 			: out std_logic;
			alu_op_out 				: out std_logic_vector(0 to 3);	  
			branch_out 				: out std_logic;
			mem_loadfile_out 		: out std_logic;
			register_a_out			: out std_logic_vector(0 to 3);
			register_b_out			: out std_logic_vector(0 to 3);
			stall					: in  std_logic
          );
end ID_EXEMEM;

architecture Behavioral of ID_EXEMEM is

begin

    process (reset,
                clk,
                flush_in,
				sig_read_data_a_in,
				sig_read_data_b_in,
				reg_dst_in, 	
				reg_write_in ,	
				alu_src_in, 
				mem_write_in,
				mem_to_reg_in, 
				alu_op_in, 
				branch_in, 
				mem_loadfile_in ) is

     -- declare variables 
	variable sig_insn : std_logic_vector(15 downto 0);
	variable sig_read_data_a : std_logic_vector(15 downto 0);
	variable sig_read_data_b : std_logic_vector(15 downto 0);
	variable reg_dst : std_logic; 
    variable reg_write : std_logic;
    variable alu_src : std_logic;
    variable mem_write : std_logic;
    variable mem_to_reg : std_logic;
	variable alu_op : std_logic_vector(0 to 3);	  
	variable branch : std_logic;
	variable mem_loadfile : std_logic; 
	variable register_a	: std_logic_vector(0 to 3);
	variable register_b	: std_logic_vector(0 to 3);
    begin            
            if (reset = '1') then
				sig_insn := X"0000";	
				sig_read_data_a := X"0000";
				sig_read_data_b := X"0000";
				reg_dst :='0' ; 
				reg_write :='0' ;
				alu_src :='0' ;
				mem_write :='0' ;
				mem_to_reg :='0' ;
				alu_op := X"0"; 
				branch :='0' ;
				mem_loadfile := '0'; 
				register_a := X"0";
				register_b := X"0";
				
            elsif (rising_edge(clk) and stall = '0') then
               if (flush_in = '1') then
				sig_insn := X"0000";
   			    sig_read_data_a := X"0000";
				sig_read_data_b := X"0000";
				reg_dst :='0' ; 
				reg_write :='0' ;
				alu_src :='0' ;
				mem_write :='0' ;
				mem_to_reg :='0' ;
				alu_op := X"0"; 
				branch :='0' ;
				mem_loadfile := '0'; 
				register_a := X"0";
				register_b := X"0";
               else
			    sig_insn := sig_insn_in;
				sig_read_data_a := sig_read_data_a_in;
				sig_read_data_b := sig_read_data_b_in;
				reg_dst := reg_dst_in; 
				reg_write := reg_write_in;
				alu_src := alu_src_in;
				mem_write := mem_write_in;
				mem_to_reg := mem_to_reg_in;
				alu_op := alu_op_in;
				branch := branch_in;
				mem_loadfile := mem_loadfile_in; 
				register_a := register_a_in;
				register_b := register_b_in;
               end if;
            end if;
		sig_insn_out		<= sig_insn;
		sig_read_data_a_out <= sig_read_data_a;
		sig_read_data_b_out <= sig_read_data_b;	
		reg_dst_out 	    <= reg_dst;    
		reg_write_out 	    <= reg_write;    
		alu_src_out 	    <= alu_src;    
		mem_write_out 	    <= mem_write;    
		mem_to_reg_out 	    <= mem_to_reg;    
		alu_op_out 	    <= alu_op;    
		branch_out 	    <= branch;    
		mem_loadfile_out    <= mem_loadfile;  
		register_a_out 		<= register_a;
		register_b_out		<= register_b;
    end process; 

end Behavioral;

