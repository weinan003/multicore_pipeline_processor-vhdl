library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity IF_ID is
    port ( reset                : in  std_logic;
           clk                  : in  std_logic;
           flush_in           	: in  std_logic;
           pc_curr_in           : in  std_logic_vector(15 downto 0);
		   sig_insn_in			: in  std_logic_vector(15 downto 0);
		   
           pc_curr_out          : out std_logic_vector(15 downto 0);
           sig_insn_out         : out std_logic_vector(15 downto 0);
		   stall					: in  std_logic);
end IF_ID;

architecture Behavioral of IF_ID is

begin

    process (reset,
                 clk,
                 flush_in,
                 pc_curr_in,
				 sig_insn_in) is

     -- declare variables
     variable pc_curr        : std_logic_vector(15 downto 0);
	 variable sig_insn       : std_logic_vector(15 downto 0);
    
    begin            
            if (reset = '1') then
               pc_curr        := X"0000";
               sig_insn		  := X"0000";
            elsif (falling_edge(clk) and stall = '0') then
               if (flush_in = '1') then
					pc_curr     := X"0000";
					sig_insn	:= X"0000";
               else
					
					pc_curr		:= pc_curr_in;
					sig_insn	:= sig_insn_in;
                end if;
            end if;
            
            pc_curr_out        <= pc_curr after 0.5 ns;
			sig_insn_out 	   <= sig_insn after 0.5 ns;
    end process; 

end Behavioral;

