---------------------------------------------------------------------------
-- instruction_memory.vhd - Implementation of A Single-Port, 16 x 16-bit
--                          Instruction Memory.
-- 
-- Notes: refer to headers in single_cycle_core.vhd for the supported ISA.
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

entity instruction_memory is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(15 downto 0);
           insn_out : out std_logic_vector(15 downto 0);
		   stall	: in  std_logic;
		   id		: in  std_logic);
end instruction_memory;

architecture behavioral of instruction_memory is
constant SIZE : integer := 255;
type mem_array is array(0 to SIZE) of std_logic_vector(15 downto 0);
signal sig_insn_mem : mem_array;


begin
    mem_process: process ( clk,
                           addr_in ) is
  
    variable var_insn_mem : mem_array;
    variable var_addr     : integer;
  
    begin
		--if(stall = '0') then
			if (reset = '1') then
				-- initial values of the instruction memory :
				--  insn_0 : load  $1, $0, 0   - load data 0($0) into $1
				--  insn_1 : load  $2, $0, 1   - load data 1($0) into $2
				--  insn_2 : add   $3, $0, $1  - $3 <- $0 - $1
				--  insn_3 : addi  $4, $1, $2  - $4 <- $1 + 2
				--  insn_4 : store $3, $0, 2   - store data $3 into 2($0)
				--  insn_5 : store $4, $0, 3   - store data $4 into 3($0)
				--  insn_6 - insn_15 : noop    - end of program
				for i in 0 to SIZE loop
					var_insn_mem(i) := std_logic_vector(to_unsigned(i, 16));
					var_insn_mem(i) := X"0000";
				 --if (not endfile(insn_file)) then
					--	readline(insn_file,line_buf);
					--	read(line_buf,data_buf);
					--	sig_insn_mem(i) <= data_buf;
				 --end if;
				end loop;
					if(id = '0') then
						var_insn_mem(0)  := X"0000"; 
						var_insn_mem(1)  := X"907F"; 
						var_insn_mem(2)  := X"1310"; 
						var_insn_mem(3)  := X"9090"; 
						var_insn_mem(4)  := X"8892"; 
						var_insn_mem(5)  := X"92E0"; 
						var_insn_mem(6)	 := X"91F0"; 
						var_insn_mem(7)	 := X"7009"; 
						var_insn_mem(8)	 := X"A212"; 
						var_insn_mem(9)  := X"823A"; 
						var_insn_mem(10) := X"9AA1"; 
						var_insn_mem(11) := X"1AB0"; 
						var_insn_mem(12) := X"19C0"; 
						var_insn_mem(13) := X"9BE0"; 
						var_insn_mem(14) := X"90F0"; 
						var_insn_mem(15) := X"502A"; 
						var_insn_mem(16) := X"9BE0"; 
						var_insn_mem(17) := X"9CF0"; 
						var_insn_mem(18) := X"601A"; 
						var_insn_mem(19) := X"9991"; 
						var_insn_mem(20) := X"99E0"; 
						var_insn_mem(21) := X"91F0"; 
						var_insn_mem(22) := X"501A"; 
						var_insn_mem(23) := X"F004"; 
						var_insn_mem(24) := X"99E0"; 
						var_insn_mem(25) := X"91F0"; 
						var_insn_mem(26) := X"601E"; 
						var_insn_mem(27) := X"9441"; 
						var_insn_mem(28) := X"0000";
						var_insn_mem(29) := X"3740";
						var_insn_mem(30) := X"8386";
						var_insn_mem(31) := X"9661"; 
						var_insn_mem(32) := X"B060"; 
						var_insn_mem(33) := X"1350"; 
						var_insn_mem(34) := X"915F";
						var_insn_mem(35) := X"98E0"; 
						var_insn_mem(36) := X"91FF"; 
						var_insn_mem(37) := X"6028"; 
						var_insn_mem(38) := X"9080";
						var_insn_mem(39) := X"F003";
						var_insn_mem(40) := X"9881";
						var_insn_mem(41) := X"F003";
						var_insn_mem(42) := X"0000";
						var_insn_mem(43) := X"F02A";
					elsif (id = '1') then
						var_insn_mem(0)  := X"0000"; 
						var_insn_mem(1)  := X"907E"; 
						var_insn_mem(2)  := X"1310"; 
						var_insn_mem(3)  := X"9090"; 
						var_insn_mem(4)  := X"8892"; 
						var_insn_mem(5)  := X"92E0"; 
						var_insn_mem(6)	 := X"91F0"; 
						var_insn_mem(7)	 := X"7009"; 
						var_insn_mem(8)	 := X"A212"; 
						var_insn_mem(9)  := X"82DA"; 
						var_insn_mem(10) := X"9AA1"; 
						var_insn_mem(11) := X"1AB0"; 
						var_insn_mem(12) := X"19C0"; 
						var_insn_mem(13) := X"9BE0"; 
						var_insn_mem(14) := X"90F0"; 
						var_insn_mem(15) := X"502A"; 
						var_insn_mem(16) := X"9BE0"; 
						var_insn_mem(17) := X"9CF0"; 
						var_insn_mem(18) := X"601A"; 
						var_insn_mem(19) := X"9991"; 
						var_insn_mem(20) := X"99E0"; 
						var_insn_mem(21) := X"91F0"; 
						var_insn_mem(22) := X"501A"; 
						var_insn_mem(23) := X"F004"; 
						var_insn_mem(24) := X"99E0"; 
						var_insn_mem(25) := X"91F0"; 
						var_insn_mem(26) := X"601E"; 
						var_insn_mem(27) := X"9441";
						var_insn_mem(28) := X"0000";					
						var_insn_mem(29) := X"3740";					
						var_insn_mem(30) := X"8D86";
						var_insn_mem(31) := X"9661"; 
						var_insn_mem(32) := X"B060"; 
						var_insn_mem(33) := X"1350"; 
						var_insn_mem(34) := X"915F";
						var_insn_mem(35) := X"98E0"; 
						var_insn_mem(36) := X"91FF"; 
						var_insn_mem(37) := X"6028"; 
						var_insn_mem(38) := X"9080";
						var_insn_mem(39) := X"F003";
						var_insn_mem(40) := X"9881";
						var_insn_mem(41) := X"F003";
						var_insn_mem(42) := X"0000";
						var_insn_mem(43) := X"F02A";
					end if;
			end if;
				-- read instructions on the rising clock edge
				var_addr := conv_integer(addr_in(7 downto 0));
				insn_out <= var_insn_mem(var_addr) after 1 ns;
			

			-- the following are probe signals (for simulation purpose)
			sig_insn_mem <= var_insn_mem;
		--end if;
    end process;
  
end behavioral;
