---------------------------------------------------------------------------
-- single_cycle_core.vhd - A Single-Cycle Processor Implementation
--
-- Notes : 
--
-- See single_cycle_core.pdf for the block diagram of this single
-- cycle processor core.
--
-- Instruction Set Architecture (ISA) for the single-cycle-core:
--   Each instruction is 16-bit wide, with four 4-bit fields.
--
--     noop      
--        # no operation or to signal end of program
--        # format:  | opcode = 0 |  0   |  0   |   0    | 
--
--     load  rt, rs, offset     
--        # load data at memory location (rs + offset) into rt
--        # format:  | opcode = 1 |  rs  |  rt  | offset |
--
--     store rt, rs, offset
--        # store data rt into memory location (rs + offset)
--        # format:  | opcode = 3 |  rs  |  rt  | offset |
--
--     add   rd, rs, rt
--        # rd <- rs + rt
--        # format:  | opcode = 8 |  rs  |  rt  |   rd   |
--		
--     addi  rt, rs, imm
--        #  rt <- rs + imm
--        #  format: | opcode = 9 |  rs  |  rt  |   imm  |
--
--	   sub	 rd, rs, rt
--		  # rd <- rs - rt
--        # format:  | opcode = A | rs   |  rt  |   rd   |
--		
--     ld_file   rt
--        # Mem[$rt] <- io_register
--		  # format:  | opcode = B |   0  |  rt  |   0   |
--
--     bge  imm
--        # if ($14 >= $15) { PC <- unsign_extend12to16b(imm) } else { PC <- PC + 1}
--        # format:  | opcode = 4 |         imm          |
--
--     beq   imm
--        # if ($14 == $15) { PC <-unsign_extend12to16b(imm) } else { PC <- PC + 1}
--		  # format:  | opcode = 5 |         imm          |
--
--     bne   imm
--        # if ($14 != $15) { PC <-unsign_extend12to16b(imm) } else { PC <- PC + 1}
--		  # format:  | opcode = 6 |         imm          |
--
--     blt   imm
--        # if ($14 < $15) { PC <-unsign_extend12to16b(imm) } else { PC <- PC + 1}
--		  # format:  | opcode = 7 |         imm          |
--
--     j    imm
--        # PC <- unsign_extend12to16b(imm)
--        # format:  | opcode = F |         imm          |
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity single_cycle_core is
    port ( reset  : in  std_logic;
           clk    : in  std_logic;
		   id 	  : in std_logic;
		   sig_grant : in std_logic;
		   sig_apply : out std_logic;
		   wb		: out 	std_logic;
		   rb		: out	std_logic;
		   ldb		: out 	std_logic;
		   addrb	: out	std_logic_vector(15 downto 0);
		   datawb	: out 	std_logic_vector(15 downto 0);
		   datarb	: in 	std_logic_vector(15 downto 0) );
end single_cycle_core;

architecture structural of single_cycle_core is

component program_counter is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(15 downto 0);
           addr_out : out std_logic_vector(15 downto 0);
		   stall	: in  std_logic);
end component;

component instruction_memory is
    port ( reset    : in  std_logic;
           clk      : in  std_logic;
           addr_in  : in  std_logic_vector(15 downto 0);
           insn_out : out std_logic_vector(15 downto 0);
		   stall	: in  std_logic;
		   id		: in  std_logic);
end component;

component sign_extend_4to16 is
    port ( data_in  : in  std_logic_vector(3 downto 0);
           data_out : out std_logic_vector(15 downto 0) );
end component;

component mux_2to1_4b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(3 downto 0);
           data_b     : in  std_logic_vector(3 downto 0);
           data_out   : out std_logic_vector(3 downto 0) );
end component;

component mux_2to1_16b is
    port ( mux_select : in  std_logic;
           data_a     : in  std_logic_vector(15 downto 0);
           data_b     : in  std_logic_vector(15 downto 0);
           data_out   : out std_logic_vector(15 downto 0) );
end component;

component control_unit is
    port ( opcode     : in  std_logic_vector(3 downto 0);
           reg_dst    : out std_logic;
           reg_write  : out std_logic;
           alu_src    : out std_logic;
           mem_write  : out std_logic;
           mem_to_reg : out std_logic;
		   alu_op 	  : out std_logic_vector(0 to 3);
		   branch 	  : out std_logic;
		   mem_loadfile:out std_logic);
end component;

component register_file is
    port ( reset           : in  std_logic;
           clk             : in  std_logic;
           read_register_a : in  std_logic_vector(3 downto 0);
           read_register_b : in  std_logic_vector(3 downto 0);
           write_enable    : in  std_logic;
           write_register  : in  std_logic_vector(3 downto 0);
           write_data      : in  std_logic_vector(15 downto 0);
           read_data_a     : out std_logic_vector(15 downto 0);
           read_data_b     : out std_logic_vector(15 downto 0);
		   stall		   : in  std_logic);
end component;

component adder_4b is
    port ( src_a     : in  std_logic_vector(3 downto 0);
           src_b     : in  std_logic_vector(3 downto 0);
           sum       : out std_logic_vector(3 downto 0);
           carry_out : out std_logic );
end component;

component adder_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
           sum       : out std_logic_vector(15 downto 0);
           carry_out : out std_logic );
end component;

component alu_16b is
    port ( src_a     : in  std_logic_vector(15 downto 0);
           src_b     : in  std_logic_vector(15 downto 0);
		   alu_op    : in  std_logic_vector(0 to 3);
           result    : out std_logic_vector(15 downto 0);
		   zerotag   : out std_logic;
           carry_out : out std_logic );
end component;

component data_memory is
    port ( reset        : in  std_logic;
           clk          : in  std_logic;
           write_enable : in  std_logic;
		   mem_loadfile : in  std_logic;
           write_data   : in  std_logic_vector(15 downto 0);
           addr_in      : in  std_logic_vector(15 downto 0);
           data_out     : out std_logic_vector(15 downto 0) );
end component;

-- stage register
component IF_ID is
    port ( reset                : in  std_logic;
           clk                  : in  std_logic;
           flush_in           	: in  std_logic;
           pc_curr_in           : in  std_logic_vector(15 downto 0);
		   sig_insn_in			: in  std_logic_vector(15 downto 0);
		   
           pc_curr_out          : out std_logic_vector(15 downto 0);
           sig_insn_out         : out std_logic_vector(15 downto 0);
		   stall		        : in  std_logic);
end component;


component ID_EXEMEM is
    port ( 	reset                	: in  std_logic;
			clk                  	: in  std_logic;
			flush_in           		: in  std_logic;
			sig_insn_in				: in std_logic_vector(15 downto 0);			
			sig_read_data_a_in 		: in std_logic_vector(15 downto 0);
			sig_read_data_b_in 		: in std_logic_vector(15 downto 0);
			reg_dst_in 				: in std_logic; 
			reg_write_in 			: in std_logic;
			alu_src_in 				: in std_logic;
			mem_write_in 			: in std_logic;
			mem_to_reg_in 			: in std_logic;
			alu_op_in 				: in std_logic_vector(0 to 3);	  
			branch_in 				: in std_logic;
			mem_loadfile_in 		: in std_logic; 
			register_a_in			: in std_logic_vector(0 to 3);
			register_b_in			: in std_logic_vector(0 to 3);
			
			sig_insn_out			: out std_logic_vector(15 downto 0);
			sig_read_data_a_out 	: out std_logic_vector(15 downto 0);
			sig_read_data_b_out 	: out std_logic_vector(15 downto 0);
			reg_dst_out 			: out std_logic; 
			reg_write_out 			: out std_logic;
			alu_src_out 			: out std_logic;
			mem_write_out 			: out std_logic;
			mem_to_reg_out 			: out std_logic;
			alu_op_out 				: out std_logic_vector(0 to 3);	  
			branch_out 				: out std_logic;
			mem_loadfile_out 		: out std_logic;
			register_a_out			: out std_logic_vector(0 to 3);
			register_b_out			: out std_logic_vector(0 to 3);
			stall		   			: in  std_logic
          );
end component;

component exemem_wb is
    port ( reset               : in  std_logic;
           clk                 : in  std_logic;
		   
           regwrt_in           : in  std_logic;
           memtoreg_in         : in  std_logic;
           data_mem_in         : in  std_logic_vector(15 downto 0);
           alu_result_in       : in  std_logic_vector(15 downto 0);
           write_register_in   : in  std_logic_vector(3 downto 0);

           regwrt_out          : out std_logic;
           memtoreg_out        : out std_logic;
           data_mem_out        : out std_logic_vector(15 downto 0);
           alu_result_out      : out std_logic_vector(15 downto 0);
           write_register_out  : out std_logic_vector(3 downto 0);
		   stall		   	   : in  std_logic		   );
end component;

component hazard is
    port ( dobranch             : in  std_logic;
           flushout             : out std_logic );
end component;

component forwarding is
    port ( register_a     : in  std_logic_vector(3 downto 0);
           register_b     : in  std_logic_vector(3 downto 0);
           write_register : in  std_logic_vector(3 downto 0);
           reg_write      : in  std_logic;
           branch         : in  std_logic;

           forwarding_a         : out std_logic;
           forwarding_b         : out std_logic );
end component;

component bus_control is
	port (	reset	: in	std_logic;
			clk		: in	std_logic;
			
			--comm to cpu
			stall	: out 	std_logic;
			r 		: in 	std_logic;
			w 		: in 	std_logic;
			ld 		: in 	std_logic;
			addr	: in 	std_logic_vector(15 downto 0);
			dataw	: in 	std_logic_vector(15 downto 0);

			--comm to bus
			grant 	: in 	std_logic;
			apply 	: out 	std_logic;
			wb		: out 	std_logic;
			rb 		: out 	std_logic;
			ldb		: out 	std_logic;
			addrb	: out	std_logic_vector(15 downto 0);
			datawb	: out 	std_logic_vector(15 downto 0)
	);
end component;

signal sig_next_pc              : std_logic_vector(15 downto 0);
signal sig_next_pc_normal       : std_logic_vector(15 downto 0);
signal sig_next_pc_branch       : std_logic_vector(15 downto 0);
signal sig_one_4b               : std_logic_vector(15 downto 0);
signal sig_pc_n_carry_out       : std_logic;

signal sig_curr_pc_IF           : std_logic_vector(15 downto 0);
signal sig_curr_pc_ID           : std_logic_vector(15 downto 0);
signal sig_insn_IF				: std_logic_vector(15 downto 0);
signal sig_insn_ID              : std_logic_vector(15 downto 0);
signal sig_insn_EXE             : std_logic_vector(15 downto 0);

signal sig_sign_extended_offset : std_logic_vector(15 downto 0);
signal sig_reg_dst_in              : std_logic;
signal sig_reg_write_in            : std_logic;
signal sig_alu_src_in              : std_logic;
signal sig_mem_write_in            : std_logic;
signal sig_mem_to_reg_in          : std_logic;
signal sig_reg_dst_out              : std_logic;
signal sig_reg_write_out            : std_logic;
signal sig_alu_src_out              : std_logic;
signal sig_mem_write_out            : std_logic;
signal sig_mem_to_reg_out           : std_logic;
signal sig_alu_op_in				: std_logic_vector(0 to 3);
signal sig_alu_op_out				: std_logic_vector(0 to 3);

signal sig_write_register       : std_logic_vector(3 downto 0);
signal sig_write_data           : std_logic_vector(15 downto 0);
signal sig_read_data_a_in          : std_logic_vector(15 downto 0);
signal sig_read_data_a_out         : std_logic_vector(15 downto 0);
signal sig_read_data_b_in          : std_logic_vector(15 downto 0);
signal sig_read_data_b_out         : std_logic_vector(15 downto 0);
signal sig_alu_src_b            : std_logic_vector(15 downto 0);
signal sig_alu_result           : std_logic_vector(15 downto 0);
signal sig_alu_carry_out        : std_logic;
signal sig_branch_in			: std_logic;
signal sig_branch_out			: std_logic;
signal sig_mem_loadfile_in		: std_logic;
signal sig_mem_loadfile_out		: std_logic;

signal sig_zerotag				: std_logic;
signal branch_select 			: std_logic;

signal register_input_a			: std_logic_vector(3 downto 0);
signal register_input_b			: std_logic_vector(3 downto 0);
signal register_input_a_out		: std_logic_vector(3 downto 0);
signal register_input_b_out		: std_logic_vector(3 downto 0);

signal sig_reg_write_WB            : std_logic;
signal sig_mem_to_reg_WB		   : std_logic;
signal sig_alu_result_WB		   : std_logic_vector(15 downto 0);
signal sig_data_mem_WB			   : std_logic_vector(15 downto 0);
signal sig_write_register_WB       : std_logic_vector(3 downto 0);

signal sig_flush 					: std_logic;
signal sig_forwarding_a				: std_logic;
signal sig_forwarding_b				: std_logic;
signal forwarding_reg_a				: std_logic_vector(15 downto 0);
signal forwarding_reg_b				: std_logic_vector(15 downto 0);

signal sig_stall					: std_logic;

begin
	--IF stage begin
    sig_one_4b <= X"0001";
    pc : program_counter
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_next_pc,
               addr_out => sig_curr_pc_IF,
			   stall	=> sig_stall); 

    next_pc_normal : adder_16b 
    port map ( src_a     => sig_curr_pc_IF, 
               src_b     => sig_one_4b,
               sum       => sig_next_pc_normal,   
               carry_out => sig_pc_n_carry_out );	
	
	
	mux_next_pc : mux_2to1_16b 
    port map ( mux_select => branch_select,
               data_a     => sig_next_pc_normal,
               data_b     => sig_next_pc_branch,
               data_out   => sig_next_pc );
	
	

    insn_mem : instruction_memory 
    port map ( reset    => reset,
               clk      => clk,
               addr_in  => sig_curr_pc_IF,
               insn_out => sig_insn_IF,
			   stall	=> sig_stall,
			   id		=> id);
	--IF stage end
	
	if_id_stage: IF_ID
	port map ( reset    => reset,
               clk      => clk,
			   flush_in => sig_flush,
               pc_curr_in  => sig_curr_pc_IF,
			   sig_insn_in => sig_insn_IF,
			   
			   pc_curr_out => sig_curr_pc_ID,
               sig_insn_out => sig_insn_ID,
			   stall		=> sig_stall);
	
	--ID stage begin
	register_input_a <= "1110" when(sig_insn_ID(15 downto 12) = "0100" or
									sig_insn_ID(15 downto 12) = "0101" or
									sig_insn_ID(15 downto 12) = "0110" or
									sig_insn_ID(15 downto 12) = "0111")
									else
					    sig_insn_ID(11 downto 8) ;	
						
	register_input_b <= "1111" when(sig_insn_ID(15 downto 12) = "0100" or
									sig_insn_ID(15 downto 12) = "0101" or
									sig_insn_ID(15 downto 12) = "0110" or
									sig_insn_ID(15 downto 12) = "0111")
									else
					    sig_insn_ID(7 downto 4) ;
						
	reg_file : register_file 
    port map ( reset           => reset, 
               clk             => clk,
               read_register_a => register_input_a,
               read_register_b => register_input_b,
               write_enable    => sig_reg_write_WB,
               write_register  => sig_write_register_WB,
               write_data      => sig_write_data,
               read_data_a     => sig_read_data_a_in,
               read_data_b     => sig_read_data_b_in,
			   stall 		   => sig_stall);
	
	ctrl_unit : control_unit 
    port map ( opcode     => sig_insn_ID(15 downto 12),
               reg_dst    => sig_reg_dst_in,
               reg_write  => sig_reg_write_in,
               alu_src    => sig_alu_src_in,
               mem_write  => sig_mem_write_in,
               mem_to_reg => sig_mem_to_reg_in,
		       alu_op 	  => sig_alu_op_in,
		       branch 	  => sig_branch_in,
			   mem_loadfile => sig_mem_loadfile_in);
	--ID stage end
	
	id_ex_stage: ID_EXEMEM port map ( 	
			reset 					=> reset,
			clk                  	=> clk,
			flush_in           		=> sig_flush,
			sig_insn_in				=> sig_insn_ID,			
			sig_read_data_a_in 		=> sig_read_data_a_in,
			sig_read_data_b_in 		=> sig_read_data_b_in,
			reg_dst_in 				=> sig_reg_dst_in,
			reg_write_in 			=> sig_reg_write_in,
			alu_src_in 				=> sig_alu_src_in,
			mem_write_in 			=> sig_mem_write_in,
			mem_to_reg_in 			=> sig_mem_to_reg_in,
			alu_op_in 				=> sig_alu_op_in,	  
			branch_in 				=> sig_branch_in,
			mem_loadfile_in 		=> sig_mem_loadfile_in,
			register_a_in			=> register_input_a,
			register_b_in			=> register_input_b,			

			sig_insn_out			=> sig_insn_EXE,
			sig_read_data_a_out 	=> sig_read_data_a_out,
			sig_read_data_b_out 	=> sig_read_data_b_out,
			reg_dst_out 			=> sig_reg_dst_out, 
			reg_write_out 			=> sig_reg_write_out,
			alu_src_out 			=> sig_alu_src_out,
			mem_write_out 			=> sig_mem_write_out,
			mem_to_reg_out 			=> sig_mem_to_reg_out,
			alu_op_out 				=> sig_alu_op_out,	  
			branch_out 				=> sig_branch_out,
			mem_loadfile_out 		=> sig_mem_loadfile_out, 
			register_a_out			=> register_input_a_out,
			register_b_out			=> register_input_b_out,
			stall		   	   		=> sig_stall			
          );
	--EX_MEM stage begin
    sign_extend : sign_extend_4to16 
    port map ( data_in  => sig_insn_EXE(3 downto 0),
               data_out => sig_sign_extended_offset );

	sig_next_pc_branch <= sig_curr_pc_ID(15 downto 12) & sig_insn_EXE(11 downto 0);

    mux_reg_dst : mux_2to1_4b 
    port map ( mux_select => sig_reg_dst_out,
               data_a     => sig_insn_EXE(7 downto 4),
               data_b     => sig_insn_EXE(3 downto 0),
               data_out   => sig_write_register );    
    
    mux_alu_src : mux_2to1_16b 
    port map ( mux_select => sig_alu_src_out,
               data_a     => sig_read_data_b_out,
               data_b     => sig_sign_extended_offset,
               data_out   => sig_alu_src_b );

    alu : alu_16b 
    port map ( src_a     => forwarding_reg_a,
               src_b     => forwarding_reg_b,
			   alu_op    => sig_alu_op_out,
               result    => sig_alu_result,
			   zerotag   => sig_zerotag,
               carry_out => sig_alu_carry_out );

    -- data_mem : data_memory 
    -- port map ( reset        => reset,
               -- clk          => clk,
               -- write_enable => sig_mem_write_out,
			   -- mem_loadfile => sig_mem_loadfile_out,
               -- write_data   => forwarding_reg_a,
               -- addr_in      => sig_alu_result,
               -- data_out     => sig_data_mem_out);
	   
	BC : bus_control 
	port map (	reset	=> reset,
			clk		 => clk,
			
			--comm to cpu
			stall	=> sig_stall,
			r 		=> sig_mem_to_reg_out,
			w 		=> sig_mem_write_out,
			ld 		=> sig_mem_loadfile_out,
			addr	=> sig_alu_result,
			dataw	=> sig_read_data_b_out,
			
			--comm to bus
			grant 	=> sig_grant,
			apply 	=> sig_apply,
			wb		=> wb,
			rb 		=> rb,
			ldb		=> ldb,
			addrb	=> addrb,
			datawb	=> datawb
	);
	
	branch_select <= (sig_branch_out and sig_zerotag);
	--EX_MEM stage end
	exemem_wb_register:exemem_wb
    port map( reset            => reset,
           clk                 => clk,
		   
           regwrt_in           => sig_reg_write_out,
           memtoreg_in         => sig_mem_to_reg_out,
           data_mem_in         => datarb,--test replace
           alu_result_in       => sig_alu_result,
           write_register_in   => sig_write_register,

           regwrt_out          => sig_reg_write_WB,
           memtoreg_out        => sig_mem_to_reg_WB,
           data_mem_out        => sig_data_mem_WB,
           alu_result_out      => sig_alu_result_WB,
           write_register_out  => sig_write_register_WB,
		   stall		   	   => sig_stall		   );
		   

		   
	--WB stage begin
	mux_mem_to_reg : mux_2to1_16b 
    port map ( mux_select => sig_mem_to_reg_WB,
               data_a     => sig_alu_result_WB,
               data_b     => sig_data_mem_WB,
               data_out   => sig_write_data );
			   
			   
	--hazard detection
	hazard_detection:hazard 
    port map ( dobranch         =>  branch_select,
			   flushout         =>	sig_flush);
		   
	forwarding_unit:forwarding
	port map(register_a   => register_input_a_out,
           register_b     => register_input_b_out,
           write_register => sig_write_register_WB,
           reg_write      => sig_reg_write_WB,
           branch         => sig_branch_out,

           forwarding_a   => sig_forwarding_a,
           forwarding_b   => sig_forwarding_b
	);
	
	forward_mux_a : mux_2to1_16b 
	port map ( 
					mux_select 	=> sig_forwarding_a,
					data_a    	=> sig_read_data_a_out, 
					data_b    	=> sig_write_data, 
					data_out  	=> forwarding_reg_a
				); 
	
	forward_mux_b : mux_2to1_16b 
	port map ( 
					mux_select 	=> sig_forwarding_b, 
					data_a    	=> sig_alu_src_b, 
					data_b    	=> sig_write_data, 
					data_out  	=> forwarding_reg_b
				); 
end structural;
