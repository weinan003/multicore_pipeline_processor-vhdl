library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity hazard is
    port ( dobranch               : in  std_logic;
           flushout             : out std_logic );
end hazard;

architecture Behavioral of hazard is

begin
    flushout <= dobranch after 0.2 ns;
end Behavioral;