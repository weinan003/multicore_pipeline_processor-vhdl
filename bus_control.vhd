---------------------------------------------------------------------------
-- data_memory.vhd - Implementation of A Single-Port, 16 x 16-bit Data
--                   Memory.
-- 
--
-- Copyright (C) 2006 by Lih Wen Koh (lwkoh@cse.unsw.edu.au)
-- All Rights Reserved. 
--
-- The single-cycle processor core is provided AS IS, with no warranty of 
-- any kind, express or implied. The user of the program accepts full 
-- responsibility for the application of the program and the use of any 
-- results. This work may be downloaded, compiled, executed, copied, and 
-- modified solely for nonprofit, educational, noncommercial research, and 
-- noncommercial scholarship purposes provided that this notice in its 
-- entirety accompanies all copies. Copies of the modified software can be 
-- delivered to persons who use it solely for nonprofit, educational, 
-- noncommercial research, and noncommercial scholarship purposes provided 
-- that this notice in its entirety accompanies all copies.
--
---------------------------------------------------------------------------
library IEEE;
library STD;
use STD.textio.all;  
use IEEE.std_logic_textio.all; 
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;
--
--		stall	r 		w 		ld 		addr 	dataw 	datar
--		^												^
--		|		|		|		|		|		|		|
--				v 		v		v 		v		v
--<--------------------------------------------------------------->bus control unit
--		^												^
--		|		|		|		|		|		|		|
--				v		v		v       v		v
--	  grant	  apply	   wb      ldb    addrb    datawb	datarb
entity bus_control is
	port (	reset	: in	std_logic;
			clk		: in	std_logic;
			
			--comm to cpu
			stall	: out 	std_logic;
			r 		: in 	std_logic;
			w 		: in 	std_logic;
			ld 		: in 	std_logic;
			addr	: in 	std_logic_vector(15 downto 0);
			dataw	: in 	std_logic_vector(15 downto 0);
			
			--comm to bus
			grant 	: in 	std_logic;
			apply 	: out 	std_logic;
			wb		: out 	std_logic;
			rb		: out   std_logic;
			ldb		: out 	std_logic;
			addrb	: out	std_logic_vector(15 downto 0);
			datawb	: out 	std_logic_vector(15 downto 0) 
	);
end bus_control;
architecture behavioral of bus_control is
TYPE state_type is (idle,busapply,datatransfer);
signal state : state_type;

signal sig_r	: std_logic;
signal sig_w	: std_logic;
signal sig_ld	: std_logic;
signal sig_addr	: std_logic_vector(15 downto 0);
signal sig_dw	: std_logic_vector(15 downto 0);
begin
	fsm : process(clk,reset)
	begin
			if (reset = '1') then
				state <=idle;
			elsif (falling_edge(clk)) then
				case state is
					when idle =>
						if (r = '1' or w = '1' or ld = '1') then
							state	<= busapply;
							
							sig_w	<= w;
							sig_r   <= r;
							sig_ld	<= ld;
							sig_addr<= addr;
							sig_dw	<= dataw;
						else
							state <= idle;
						end if;
					when busapply =>
						if (grant = '1') then
							state <= datatransfer;
						else
							state <= busapply;
						end if;
					when datatransfer =>
						if (r = '1' or w = '1' or ld = '1') then
							state	<= busapply;
							
							sig_w	<= w;
							sig_r   <= r;
							sig_ld	<= ld;
							sig_addr<= addr;
							sig_dw	<= dataw;
						else
							state <= idle;
						end if;

				end case;
			end if;
	end process;
    
	control: process(state)
	begin
		--addrb	<= X"0000";
		rb <= '0';
		wb <= '0';
		ldb <= '0';
		case state is 
			when busapply =>
				stall <= '1';
				apply <= '1';
			when datatransfer =>
				apply <= '1';
				stall <= '0';
				
				rb		<= sig_r;
				wb		<= sig_w;
				ldb		<= sig_ld;
				addrb 	<= sig_addr;
				if( sig_w = '1' )then
					datawb	<= sig_dw;
				end if;
			when idle =>
				stall <= '0';
				apply <= '0';
		end case;
	end process;
end behavioral;
