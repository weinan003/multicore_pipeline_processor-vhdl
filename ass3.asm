addi	$7,$0,(F/E) 'depend on cpu id
ld 		$pattern,0($3)
loop_1: addi	$index,$0,0		'$index <- 0
loop_2:	add 	$sum,$tag,$index		
addi 	$E,$sum,0S	
addi	$F,$pattern_size,0
blt 	map_ret						
sub 	$sum,$sum,$pattern_size
map_ret:    add	$offset,$sum,($3/$13)		'depend on cpu id		'因为string的buffer是从内存的第六十五个偏移开始的
addi 	$offset,$offset,1
ld 		$temp1,0($offset)				'Mem[offset]->string buffer
ld		$temp2,0($index)				'Mem[index]->pattern
addi 	$E,$temp1,0S	
addi	$F,$0,0
beq 	finish 				'if(Mem[offset] == EOF) goto finish;
addi 	$E,$temp1,0	
addi	$F,$temp2,0			
bne 	loop_2_done		'if(Mem[index] != Mem[offset]) break;
addi 	$index,$index,1					' index ++ in for loop
addi 	$E,$index,0	
addi	$F,$pattern_size,0
beq		loop_2_done 'if index == 64 break;
j		$loop_2							' while(true	
addi 	$E,$index,0	
addi	$F,$pattern_size,0
loop_2_done:bne 	 tagmov
addi	$result,$result,1   --$4

st 		$result,0($7)
tagmov:add     $6,($3/$13),$tag 		'depend on cpu id
addi	$6,$6,1
ld_file 0($6)				'将file中一个char 加载到tag指向的内存地址中
ld 		$5,0($3) 			'将pattern length 从内存中load到寄存器内
addi 	$5,$5,-1
addi 	$E,$tag,0	
addi	$F,$5,0
bne		unrefresh_tag
addi	$tag,$0,0
j		loop_1
unrefresh_tag:addi 	$tag,$tag,1
j 		loop_1
finish: 
j 		finish







$1 pattern(pattern_size)
$2 sum
$3 memory address for pattern size
$4 result
$5
$8 tag
$9 index
$10 offset
$11 temp1
$12 temp2
$13 loopborder
$14,15:compare register
